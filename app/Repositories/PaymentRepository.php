<?php
	namespace App\Repositories;

	use App\Models\Payment;

	/**
	 * Class PaymentRepository
	 * @package App\Repositories
	 */
	class PaymentRepository {
		public function __construct(Payment $model) {
			$this->model = $model;
		}

		/**
		 * @param $id
		 * @return mixed
		 */
		public function find($id) {
			return $this->model::find($id);
		}

		/**
		 * @param array $filters
		 * @param false $queryOnly
		 * @return mixed
		 */
		public function get($filters = [], $queryOnly = false){
			$payments = $this->model::where('id', '<>',''); //calling static on non-static
//            if($filters){
			foreach($filters as $key => $filter){
				$payments = $payments->where($key, $filter);
			}
			return ($queryOnly) ? $payments : $payments->get();
		}

		/**
		 * @param $data
		 * @return mixed
		 */
		public function create($data){
			return $this->model->create($data);
		}

		/**
		 * @param $id
		 * @param $data
		 * @return mixed
		 */
		public function update($id, $data){
			return $this->model->where('id', $id)->update($data);
		}
	}
