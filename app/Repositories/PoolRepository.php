<?php
	namespace App\Repositories;

	/**
	 * Class PoolRepository
	 * @package App\Repositories
	 */
	class PoolRepository{

		public $model;

		public function __construct(\App\Models\Pool $model) {
			$this->model = $model;
		}

		/**
		 * @param $id
		 * @param null $user
		 * @param null $poolType
		 * @param array $with
		 * @return mixed
		 */
		public function load_pool($id,
		                          $user = null,
		                          $poolType = null,
		                          $with = []
		){
			if($poolType) { //filter by specific type
				$this->model = $this->model->with($with);
			}
			if($user) { //filter by specific user
				$this->model = $this->model->whereHas('users', function($query) use ($user){
					$query->where('user_id', $user->id);
				});
			}
			return $this->find($id);

		}

		/**
		 * @param $pool
		 * @param $userId
		 * @return mixed
		 */
		public function is_pool_commissioner(&$pool, $userId)
		{
			return  $pool->commissioners->where('id', $userId)->count();
		}

		/**
		 * @param $id
		 * @return mixed
		 */
		public function find($id)
		{
			return $this->model->find($id);
		}

		/**
		 * @param $puid
		 * @return mixed
		 */
		public function find_by_puid($puid){
			return $this->model->where('puid', $puid)->first();
		}

		/**
		 * @param $hash
		 * @return mixed
		 */
		public function find_by_hash($hash){
			return $this->model->where('hash', $hash)->first();
		}

		/**
		 * @param $id
		 * @param $userId
		 * @return mixed
		 */
		public function get_commissioner_pool($id, $userId)
		{
			return $this->model->whereHas('commissioners', function($query) use ($userId) {
				$query->where('user_id', $userId);
			})->where('id',$id)->first();
		}

		/**
		 * @param array $filters
		 * @param false $queryOnly
		 * @return mixed
		 */
		public function get($filters = [], $queryOnly = false){
			$pools = $this->model::where('id', '<>',''); //calling static on non-static
//            if($filters){
			foreach($filters as $key => $filter){
				$pools = $pools->where($key, $filter);
			}
			return ($queryOnly) ? $pools : $pools->get();
		}

		/**
		 * @param false $id
		 * @param false $user
		 * @param false $queryOnly
		 * @return \App\Models\Pool
		 */
		public function get_pool($id = false, $user = false, $queryOnly = false)
		{
			$pools = $this->model;
			if($id){
				$pools = $pools->where('id', $id);
			}
			if($user){
				$pools = $pools->whereHas('users', function($query) use ($user){
					$query->where('users.id',$user->id);
				});
			}
			if($queryOnly) {
				return $pools;
			}
			return ($id) ? $pools->first() : $pools->get();
		}

		/**
		 * @param array $data
		 * @return mixed
		 */
		public function create($data = [])
		{
			return $this->model::create($data);
		}

		/**
		 * @param $id
		 * @return mixed
		 */
		public function delete($id)
		{
			return $this->model::find($id)->delete();
		}

		/**
		 * @param $id
		 * @param $data
		 * @return mixed
		 */
		public function update($id, $data)
		{
			return $this->model::where('id',$id)->update($data);
		}

		/**
		 * @param $term
		 * @param $take
		 * @return \App\Models\Pool[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
		 */
		public function search($term, $take){
			$pools = $this->model::with(['pool_type', 'commissioner'])
				->whereRaw("MATCH (pool_name) AGAINST (? IN NATURAL LANGUAGE MODE)", $term)
				->orderBy('id','desc');
			if($take){
				$pools->take($take);
			}
			return $pools->get();
		}


	}
