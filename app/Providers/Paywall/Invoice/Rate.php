<?php
	namespace App\Providers\Paywall\Invoice;
	use App\Repositories\RateRepository;

	/**
	 * Class Rate
	 * @package App\Providers\Paywall\Invoice
	 */
	class Rate {

		private $_rateRepo;

		/**
		 * Rate constructor.
		 * @param RateRepository $rateRepo
		 */
		public function __construct(RateRepository $rateRepo) {
			$this->_rateRepo = $rateRepo;
		}

		/**
		 * @param $id
		 * @return mixed
		 */
		public function find($id){
			return $this->_rateRepo->find($id);
		}

		/**
		 * @param array $filters
		 * @param false $queryOnly
		 * @return mixed
		 */
		public function get($filters = [], $queryOnly = false){
			return $this->_rateRepo->get($filters, $queryOnly);
		}

		/**
		 * @param $data
		 * @return mixed
		 */
		public function create($data){
			return $this->_rateRepo->create($data);
		}

		/**
		 * @param $id
		 * @param $data
		 * @return mixed
		 */
		public function update($id, $data){
			return $this->_rateRepo->update($id, $data);
		}
	}
