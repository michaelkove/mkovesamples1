<?php
    namespace App\Providers\Paywall\Gateway;
    use net\authorize\api\constants\ANetEnvironment;
    use net\authorize\api\contract\v1 as AnetAPI;
    use net\authorize\api\controller as AnetController;
    use App\Providers\Paywall\Invoice\Invoice as InvoiceProvider;
    use App\Providers\Paywall\Invoice\Payment as PaymentProvider;

    class AuthorizeNetProviderSingleCharge {

        private $_invoiceProvider;
        private $_paymentProvider;
        private $_authNetCommonProvider;
        private $_endPoint;

        function __construct(
            InvoiceProvider $ip,
            PaymentProvider $pp,
            AuthorizeNetCommon $_authNetCommonProvider
        ) {
            $this->_authNetCommonProvider = $_authNetCommonProvider;
            $this->_paymentProvider = $pp;
            $this->_invoiceProvider = $ip;
            $this->_endPoint = (__conf('authorizeNet.settings.live_environment','boolean', false)) ? ANetEnvironment::PRODUCTION : ANetEnvironment::SANDBOX;
        }

        public function pay($amount, &$invoice, &$user, $ccData = []){
            $chargeData = [
                'cc_number' => $ccData['cc_number'],
                'exp' => $ccData['exp'],
                'csv' => $ccData['csv'],
                'user_id' => $user->id,
                'email' => $user->email,
                'type' => "individual",
                'invoice_number' => $invoice->id,
                'description' => $invoice->pool->pool_name,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'company' => '',
                'address' => '',
                'city' => '',
                'state' => '',
                'zip' => '',
                'country_code' => 'USA',
                'amount' => $amount,
            ];

            $response = $this->_charge_credit_card($chargeData);
            $responseData = $this->_parse_response_data($response);

            if($responseData['error']){
                $returnData = [
                    'error' => true,
                    'message' => $responseData['message']
                ];
            }else {
                $invoiceUpdateData = [
                    'paid_at' => date('Y-m-d H:i:s'),
                    'paid' => true
                ];
                $userId = \Auth::user()->id;
                $this->_invoiceProvider->update($invoice->id, $invoiceUpdateData);
                if(__conf('authorizeNet.settings.charge_test_amount','boolean',true)){
                    $description = "Test amount of $1.99 was charged. Change config for real amounts";
                } else {
                    $description = $responseData['description'];
                }
                $paymentData = [
                    'invoice_id' => $invoice->id,
                    'user_id' => $userId,
                    'gateway_id' => 1,
                    'provider_response' => $responseData['code'],
                    'gateway_payment_id' => $responseData['txId'],
                    'provider' => "Authorize.net",
                    'paid_at' => date('Y-m-d H:i:s'),
                    'token' => $responseData['auth_code'],
                    'amount' => $amount,
                    'note' => $description
                ];
                $payment = $this->_paymentProvider->create($paymentData);
                $returnData = [
                    'error' => false,
                    'message' => $responseData['message'],
                    'payment' => $payment,
                ];
            }
            return $returnData;
        }

        private function _charge_credit_card($data)
        {
            $merchantAuthentication = $this->_authenticate();
            $refId = 'PSP-' . time();
            $creditCard = $this->_set_credit_card($data);
            $paymentOne = $this->_create_payment($creditCard);
            $order = $this->_create_order($data);
            $customerAddress = $this->_create_customer($data);
            $customerData = $this->_set_customer_data_type($data);
            $duplicateWindowSetting = $this->_create_duplicate_settings($data);

            //TODO: Remove for prod
            if(__conf('authorizeNet.settings.charge_test_amount','boolean',true)){
                $data['amount'] = 1.99;
            }


            $transactionRequestType = $this->_create_transaction_request_type($data['amount'], $order, $paymentOne, $customerAddress, $customerData, $duplicateWindowSetting);
            $request = $this->_create_request($merchantAuthentication, $refId, $transactionRequestType);
            $response = $this->_get_response($request);

            return $response;
        }

        private function _authenticate(){
            return $this->_authNetCommonProvider->_authenticate();
//            $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
//            $login = __conf('authorizeNet.settings.api_login','text', '8pdbF9X6u');
//            $token = __conf('authorizeNet.settings.api_transaction_key','text', '4e6Ry2GWvNu735p3');
//            $merchantAuthentication->setName($login);
//            $merchantAuthentication->setTransactionKey($token);
//            return $merchantAuthentication;
        }

        private function _set_credit_card(&$data){
            $creditCard = new AnetAPI\CreditCardType();
            $creditCard->setCardNumber($data['cc_number']);
            $creditCard->setExpirationDate($data['exp']);
            $creditCard->setCardCode($data['csv']);
            return $creditCard;
        }

        private function _create_payment(&$creditCard){
            $paymentOne = new AnetAPI\PaymentType();
            $paymentOne->setCreditCard($creditCard);
            return $paymentOne;
        }

        private function _create_order(&$data){
            $order = new AnetAPI\OrderType();
            $order->setInvoiceNumber($data['invoice_number']);
            $order->setDescription($data['description']);
            return $order;
        }

        private function _create_customer(&$data){
            $customerAddress = new AnetAPI\CustomerAddressType();
            $customerAddress->setFirstName($data['first_name']);
            $customerAddress->setLastName($data['last_name']);
            $customerAddress->setCompany($data['company']);
            $customerAddress->setAddress($data['address']);
            $customerAddress->setCity($data['city']);
            $customerAddress->setState($data['state']);
            $customerAddress->setZip($data['zip']);
            $customerAddress->setCountry($data['country_code']);
            return $customerAddress;
        }

        private function _set_customer_data_type(&$data){
            $customerData = new AnetAPI\CustomerDataType();
            $customerData->setType($data['type']);
            $customerData->setId($data['user_id']);
            $customerData->setEmail($data['email']);
            return $customerData;
        }

        private function _create_duplicate_settings(&$data){
            // Add values for transaction settings
            $duplicateWindowSetting = new AnetAPI\SettingType();
            $duplicateWindowSetting->setSettingName("duplicateWindow");
            $duplicateWindowSetting->setSettingValue("60");
            return $duplicateWindowSetting;
        }

        private function _create_transaction_request_type(&$amount, &$order, &$paymentOne, &$customerAddress, &$customerData, &$duplicateWindowSetting){

            $transactionRequestType = new AnetAPI\TransactionRequestType();
            $transactionRequestType->setTransactionType("authCaptureTransaction");
            $transactionRequestType->setAmount($amount);
            $transactionRequestType->setOrder($order);
            $transactionRequestType->setPayment($paymentOne);
            $transactionRequestType->setBillTo($customerAddress);
            $transactionRequestType->setCustomer($customerData);
            $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
            return $transactionRequestType;
        }

        private function _create_request(&$merchantAuthentication, &$refId, &$transactionRequestType){
            $request = new AnetAPI\CreateTransactionRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setRefId($refId);
            $request->setTransactionRequest($transactionRequestType);
            return $request;
        }

        private function _get_response(&$request){

            $controller = new AnetController\CreateTransactionController($request);

//            $response = $controller->executeWithApiResponse($env);
            $response = $controller->executeWithApiResponse($this->_endPoint);
            return $response;
        }

        private function _parse_response_data(&$response){
            $data = [
                'error' => false,
                'message' => "",
                'txId' => '',
                'code' => '',
                'auth_code' => '',
                'description' => '',
            ];
            if ($response != null) {
                if ($response->getMessages()->getResultCode() == "Ok") {
                    $tresponse = $response->getTransactionResponse();
                    if ($tresponse != null && $tresponse->getMessages() != null) {
                        $data['txId'] = $tresponse->getTransId();
                        $data['code'] = $tresponse->getResponseCode();
                        $data['auth_code'] = $tresponse->getAuthCode();
                        $data['description'] = $tresponse->getMessages()[0]->getDescription();
                        $data['message'] = "OK";
                    } else {
                        $data['error'] = true;
                        if ($tresponse->getErrors() != null) {
                            $data['message'] = $tresponse->getErrors()[0]->getErrorText();
                        }
                    }
                } else {
                    $data['error'] = true;
                    $tresponse = $response->getTransactionResponse();
                    if ($tresponse != null && $tresponse->getErrors() != null) {
                        $data['message'] = $tresponse->getErrors()[0]->getErrorText();
                    } else {
                        $data['message'] = $response->getMessages()->getMessage()[0]->getText();
                    }
                }
            } else {
                $data['message'] = "No response returned";
                $data['error'] = true;
            }
            return $data;

        }
    }
