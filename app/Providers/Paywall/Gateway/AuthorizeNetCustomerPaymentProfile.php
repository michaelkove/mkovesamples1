<?php
    namespace App\Providers\Paywall\Gateway;
    use App\Providers\User\User as UserProvider;
    use net\authorize\api\contract\v1 as AnetAPI;
    use net\authorize\api\controller as AnetController;
    use \net\authorize\api\constants\ANetEnvironment;

    class AuthorizeNetCustomerPaymentProfile {

        private $_userProvider;
        private $_response;
        private $_authNetCommonProvider;
        private $_endPoint;

        function __construct(
            UserProvider $_userProvider,
            AuthorizeNetCommon $_authNetCommonProvider
        ) {
            $this->_userProvider = $_userProvider;
            $this->_authNetCommonProvider = $_authNetCommonProvider;
            $this->_response = [
                'error' => true,
                'message' => "Error occurred: information missing",
                'code' => '0000',
                'data' => null,
            ];
            $this->_endPoint = (__conf('authorizeNet.settings.live_environment','boolean', false)) ? ANetEnvironment::PRODUCTION : ANetEnvironment::SANDBOX;
        }

        public function create_customer_payment_profile($id, $billingData = []){
            try {
                $merchantAuth = $this->_authenticate();
                $authorizeNetCustomerProfileProvider = resolve(AuthorizeNetCustomerProfile::class);
                $data = $authorizeNetCustomerProfileProvider->get_customer_profile($id);
                if($data && isset($data['data']['profile'])){
                    $customerProfile = $data['data']['profile'];
                    $id = $customerProfile->getCustomerProfileId();
                    $creditCard = $this->_create_credit_card($billingData);

                    if($creditCard){
                        $paymentCreditCard = new AnetAPI\PaymentType();
                        $paymentCreditCard->setCreditCard($creditCard);
                        // Create the Bill To info for new payment type
                        $billTo = $this->_create_customer_address($billingData);
                        $default = $billingData['default'];
                        $paymentProfile = $this->_create_payment_profile($billTo, $paymentCreditCard, $default);
                        $response = $this->_create_payment_profile_request($merchantAuth, $id, $paymentProfile);
                        $this->_parse_create_payment_profile_response($response);
                    }

                }
            } catch (\Exception $e){
//                dd($e->getMessage(), $e->getTrace());
            }

            return $this->_response;
        }

        public function get_customer_payment_profile($customerProfileId, $id){
            $merchantAuthentication = $this->_authenticate();
            $request = $this->_create_get_payment_profile_request($merchantAuthentication, $customerProfileId, $id);
            $controller = new AnetController\GetCustomerPaymentProfileController($request);
            $response = $controller->executeWithApiResponse($this->_endPoint);
            $this->_parse_get_payment_profile_response($response);
            return $this->_response;
        }

        public function delete_customer_payment_profile($customerProfileId, $id){

            $merchantAuthentication = $this->_authenticate();
            $response = $this->_create_delete_payment_profile_request($merchantAuthentication, $customerProfileId, $id);
            $this->_parse_delete_payment_profile_response($response);
            return $this->_response;
        }

        public function update_customer_payment_profile($customerProfileId, $id, &$billingData){

            $merchantAuthentication = $this->_authenticate();
            $paymentProfileData = $this->get_customer_payment_profile($customerProfileId, $id);
            if($paymentProfileData['error']){
                return $this->_response;
            }
            $paymentProfile = $paymentProfileData['data'];

            $response = $this->_create_update_payment_profile_request($merchantAuthentication, $customerProfileId, $paymentProfile);
            $this->_parse_update_payment_profile_response($response);
            return $this->_response;
        }

        private function _authenticate(){
            return $this->_authNetCommonProvider->_authenticate();
        }

        private function _create_credit_card(&$billingData){
            // Set credit card information for payment profile
            $expDate = (isset($billingData['yyyy']) && isset($billingData['mm'])) ? $billingData['yyyy']."-".$billingData['mm'] : false;
            $ccNumber = (isset($billingData['cc_number'])) ? $billingData['cc_number'] : false;
            $csv = (isset($billingData['csv'])) ? $billingData['csv'] : false;
            if($expDate && $ccNumber && $csv){
                $creditCard = new AnetAPI\CreditCardType();
                $creditCard->setCardNumber($ccNumber);
                $creditCard->setExpirationDate($expDate);
                $creditCard->setCardCode($csv);
                return $creditCard;
            }
            $this->_response['message'] = "Invalid credit card information";
            $this->_response['error'] = true;
            return false;
        }

        private function _create_customer_address(&$billingData){
            // set billto
            $firstName = isset($billingData['first_name']) ? $billingData['first_name'] : false;
            $lastName = isset($billingData['last_name']) ? $billingData['last_name'] : false;
            $company = isset($billingData['company']) ? $billingData['company'] : false;
            $street = isset($billingData['street_1']) ? $billingData['street_1'] : false;
            $city = isset($billingData['city']) ? $billingData['city'] : false;
            $state = isset($billingData['state']) ? $billingData['state'] : false;
            $zip = isset($billingData['zip']) ? $billingData['zip'] : false;
            $country = isset($billingData['country']) ? $billingData['country'] : false;
            $phone = isset($billingData['phone']) ? $billingData['phone'] : false;
            if($firstName && $lastName && $street && $city && $state && $zip && $phone && $country){
                $billTo = new AnetAPI\CustomerAddressType();
                $billTo->setFirstName($firstName);
                $billTo->setLastName($lastName);
                $billTo->setCompany($company);
                $billTo->setAddress($street);
                $billTo->setCity($city);
                $billTo->setState($state);
                $billTo->setZip($zip);
                $billTo->setCountry($country);
                $billTo->setPhoneNumber($phone);
                return $billTo;
            }
            $this->_response['message'] = "Missing Billing Information";
            $this->_response['error'] = true;
            return false;
        }

        private function _create_customer_shipping_address(&$billingData){
            // set billto
            $firstName = isset($billingData['first_name']) ? $billingData['first_name'] : false;
            $lastName = isset($billingData['last_name']) ? $billingData['last_name'] : false;
            $company = isset($billingData['company']) ? $billingData['company'] : false;
            $street = isset($billingData['street_1']) ? $billingData['street_1'] : false;
            $city = isset($billingData['city']) ? $billingData['city'] : false;
            $state = isset($billingData['state']) ? $billingData['state'] : false;
            $zip = isset($billingData['zip']) ? $billingData['zip'] : false;
            $country = isset($billingData['country']) ? $billingData['country'] : false;
            $phone = isset($billingData['phone']) ? $billingData['phone'] : false;
            if($firstName && $lastName && $street && $city && $state && $zip && $phone && $country){
                $customerShippingAddress = new AnetAPI\CustomerAddressType();
                $customerShippingAddress->setFirstName($firstName);
                $customerShippingAddress->setLastName($lastName);
                $customerShippingAddress->setCompany($company);
                $customerShippingAddress->setAddress($street);
                $customerShippingAddress->setCity($city);
                $customerShippingAddress->setState($state);
                $customerShippingAddress->setZip($zip);
                $customerShippingAddress->setCountry($country);
                $customerShippingAddress->setPhoneNumber($phone);
                return $customerShippingAddress;
            }
            $this->_response['message'] = "Invalid credit card information";
            $this->_response['error'] = true;
            return false;

        }

        private function _create_payment_profile($billTo, &$paymentCreditCard, $default = false){

            $paymentProfile = new AnetAPI\CustomerPaymentProfileType();
            $paymentProfile->setCustomerType('individual');
            $paymentProfile->setBillTo($billTo);
            $paymentProfile->setPayment($paymentCreditCard);
            $paymentProfile->setDefaultPaymentProfile($default);
            return $paymentProfile;
        }

        private function _create_payment_profile_request($merchantAuthentication, $profileId, $paymentProfile){
            $paymentProfileRequest = new AnetAPI\CreateCustomerPaymentProfileRequest();
            $paymentProfileRequest->setMerchantAuthentication($merchantAuthentication);

            // Add an existing profile id to the request
            $paymentProfileRequest->setCustomerProfileId($profileId);
            $paymentProfileRequest->setPaymentProfile($paymentProfile);
            $paymentProfileRequest->setValidationMode("liveMode");

            // Create the controller and get the response
            $controller = new AnetController\CreateCustomerPaymentProfileController($paymentProfileRequest);
            return $controller->executeWithApiResponse($this->_endPoint);
        }

        private function _create_get_payment_profile_request($merchantAuthentication, $profileId, $id){
            $refId = 'ref' . time();
            $request = new AnetAPI\GetCustomerPaymentProfileRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setRefId( $refId);
            $request->setCustomerProfileId($profileId);
            $request->setCustomerPaymentProfileId($id);
            return $request;
        }

        private function _create_update_payment_profile_request($merchantAuthentication, $profileId, $paymentProfile){
            $request = new AnetAPI\UpdateCustomerPaymentProfileRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setCustomerProfileId($profileId);
            $request->setPaymentProfile( $paymentProfile );
            $controller = new AnetController\UpdateCustomerPaymentProfileController($request);
            return $controller->executeWithApiResponse( $this->_endPoint);

        }

        private function _create_delete_payment_profile_request($merchantAuthentication, $profileId, $paymentProfileId){
            $request = new AnetAPI\DeleteCustomerPaymentProfileRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setCustomerProfileId($profileId);
            $request->setCustomerPaymentProfileId($paymentProfileId);
            $controller = new AnetController\DeleteCustomerPaymentProfileController($request);
            return $controller->executeWithApiResponse( $this->_endPoint);
        }

        private function _parse_create_payment_profile_response(&$response){
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                $this->_response['error'] = false;
                $this->_response['message'] = "Successfully created customer payment profile : " . $response->getCustomerProfileId() . "\n";
                $this->_response['code'] = '200';
                $this->_response['data'] = $response->getCustomerPaymentProfileId();
            } else {
                $errorMessages = $response->getMessages()->getMessage();
                $this->_response['error'] = true;
                $this->_response['message'] =  "Payment: ".$errorMessages[0]->getText();
                $this->_response['code'] = $errorMessages[0]->getCode();
                $this->_response['data'] = null;
            }
            return true;
        }

        private function _parse_get_payment_profile_response(&$response){
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                $this->_response['error'] = false;
                $this->_response['message'] = "Successfully got customer payment profile : " . $response->getPaymentProfile()->getCustomerPaymentProfileId() . "\n";
                $this->_response['code'] = '200';
                $this->_response['data'] = $response->getPaymentProfile();
            } else {
                $errorMessages = $response->getMessages()->getMessage();
                $this->_response['error'] = true;
                $this->_response['message'] =  "Payment: ".$errorMessages[0]->getText();
                $this->_response['code'] = $errorMessages[0]->getCode();
                $this->_response['data'] = null;
            }
            return true;
        }

        private function _parse_update_payment_profile_response(&$response){
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                $this->_response['error'] = false;
                $this->_response['message'] = "Successfully updated customer payment profile : " . $response->getPaymentProfile()->getCustomerPaymentProfileId() . "\n";
                $this->_response['code'] = '200';
                $this->_response['data'] = $response->getPaymentProfile();
            } else {
                $errorMessages = $response->getMessages()->getMessage();
                $this->_response['error'] = true;
                $this->_response['message'] =  "Payment: ".$errorMessages[0]->getText();
                $this->_response['code'] = $errorMessages[0]->getCode();
                $this->_response['data'] = null;
            }
            return true;
        }

        private function _parse_delete_payment_profile_response(&$response){
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                $this->_response['error'] = false;
                $this->_response['message'] = "Successfully deleted payment profile";
                $this->_response['code'] = '200';
                $this->_response['data'] = null;
            } else {
                $errorMessages = $response->getMessages()->getMessage();
                $this->_response['error'] = true;
                $this->_response['message'] =  "Payment: ".$errorMessages[0]->getText();
                $this->_response['code'] = $errorMessages[0]->getCode();
                $this->_response['data'] = null;
            }
            return true;
        }
    }
