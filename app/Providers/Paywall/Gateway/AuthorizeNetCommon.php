<?php
    namespace App\Providers\Paywall\Gateway;
    use App\Providers\User\User as UserProvider;
    use net\authorize\api\contract\v1 as AnetAPI;
    use net\authorize\api\controller as AnetController;

    class AuthorizeNetCommon {


        function __construct() {

        }

        public function _authenticate(){
            $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
            $login = __conf('authorizeNet.settings.api_login','text', 'xxxxxxxxxxxxxxxxxxxx');
            $token = __conf('authorizeNet.settings.api_transaction_key','text', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxx');
            $merchantAuthentication->setName($login);
            $merchantAuthentication->setTransactionKey($token);
            return $merchantAuthentication;
        }
    }
