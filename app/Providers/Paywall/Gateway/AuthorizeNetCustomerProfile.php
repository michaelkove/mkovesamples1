<?php
    namespace App\Providers\Paywall\Gateway;
    use App\Providers\User\User as UserProvider;
    use net\authorize\api\constants\ANetEnvironment;
    use net\authorize\api\contract\v1 as AnetAPI;
    use net\authorize\api\controller as AnetController;

    class AuthorizeNetCustomerProfile {

        private $_userProvider;
        private $_authNetCommonProvider;
        private $_response;
        private $_endPoint;

        function __construct(
            UserProvider $_userProvider,
            AuthorizeNetCommon $_authNetCommonProvider
        ) {
            $this->_userProvider = $_userProvider;
            $this->_authNetCommonProvider = $_authNetCommonProvider;
            $this->_response = [
                'error' => true,
                'message' => "Error occurred",
                'code' => '0000',
                'data' => null,
            ];
            $this->_endPoint = (__conf('authorizeNet.settings.live_environment','boolean', false)) ? ANetEnvironment::PRODUCTION : ANetEnvironment::SANDBOX;
        }

        public function create_customer_profile(&$user, $billingData = [])
        {

            $merchantAuthentication = $this->_authenticate();
            // Set the transaction's refId
            $refId = 'ref-'.$user->id."-".time();

            //see if they provided an email address
            $email = $billingData['email'] ?? $user->email;
            $customerProfile = $this->_create_customer_profile($user, $email);

//
//            if(count($shippingProfiles)){
//                $customerProfile->setShipToList($shippingProfiles);
//            }
//            if(count($paymentProfiles)){
//                $customerProfile->setpaymentProfiles($paymentProfiles);
//            }

            // Assemble the complete transaction request
            $request = $this->_create_customer_profile_request($merchantAuthentication, $refId, $customerProfile);

            // Create the controller and get the response
            $response = $this->_send_request($request);

            $this->_parse_create_response($response);

            return $this->_response;

        }

        public function update_email_on_customer_profile($id, $user, $email){

            $customerProfile = $this->get_customer_profile($id);
            $email = $email ?? $user->email;
            $updateCustomerProfile = $this->_update_customer_profile($customerProfile, $email);
            $merchantAuthentication = $this->_authenticate();
            $response = $this->_update_customer_profile_request($merchantAuthentication, $updateCustomerProfile);

           $this->_parse_update_customer_profile_response($response);
           if(!$this->_response['error']){
               $this->get_customer_profile($id);
               $this->_response['message'] = "Updated email";
           }
           return $this->_response;
        }

        public function delete_customer_profile($id){
            $merchantAuthentication = $this->_authenticate();
            $response = $this->_delete_customer_profile_request($merchantAuthentication, $id);
            $this->_parse_delete_customer_profile_response($response);
            return $this->_response;
        }

        public function get_customer_profile($profileIdRequested){
            if($profileIdRequested){
                $merchantAuthentication = $this->_authenticate();
                $request = new AnetAPI\GetCustomerProfileRequest();
                $request->setMerchantAuthentication($merchantAuthentication);
                $request->setCustomerProfileId($profileIdRequested);

                $controller = new AnetController\GetCustomerProfileController($request);
                $response = $controller->executeWithApiResponse( $this->_endPoint);

                $this->_parse_get_response($response);
                return $this->_response;
            }

        }



        private function _authenticate(){
            return $this->_authNetCommonProvider->_authenticate();
        }

        private function _create_customer_profile(&$user, $email){
            $uniqueUserId = "PSP-".$user->id."-".time();
            $customerProfile = new AnetAPI\CustomerProfileType();
            $customerProfile->setDescription("User created on ".date('Y-m-d H:i:s')." by system");
            $customerProfile->setMerchantCustomerId($uniqueUserId);
            $customerProfile->setEmail($email);

            return $customerProfile;
        }

        private function _update_customer_profile($profileCreated, $email){
            $updateCustomerProfile = new AnetAPI\CustomerProfileExType();
            $updateCustomerProfile->setCustomerProfileId($profileCreated);
            $updateCustomerProfile->setDescription("Updated Profile On ".date('Y-m-d H:i:s'));
            $updateCustomerProfile->setEmail($email);
            return $updateCustomerProfile;
        }

        private function _create_customer_profile_request(&$merchantAuthentication, $refId, &$customerProfile){
            $request = new AnetAPI\CreateCustomerProfileRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setRefId($refId);
            $request->setProfile($customerProfile);
            return $request;
        }

        private function _update_customer_profile_request(&$merchantAuthentication, $updateCustomerProfile){
            $request = new AnetAPI\UpdateCustomerProfileRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setProfile($updateCustomerProfile);
            $controller = new AnetController\UpdateCustomerProfileController($request);
            $response = $controller->executeWithApiResponse( $this->_endPoint);
        }

        private function _delete_customer_profile_request(&$merchantAuthentication, $customerProfileId){
            $request = new AnetAPI\DeleteCustomerProfileRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setCustomerProfileId( $customerProfileId );
            $controller = new AnetController\DeleteCustomerProfileController($request);
            return $controller->executeWithApiResponse( $this->_endPoint);
        }

        private function _send_request(&$request){
            $controller = new AnetController\CreateCustomerProfileController($request);
            return $controller->executeWithApiResponse($this->_endPoint);

        }

        private function _parse_create_response(&$response){

            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                $this->_response['error'] = false;
                $this->_response['message'] = "Successfully created customer profile : " . $response->getCustomerProfileId() . "\n";
                $this->_response['code'] = '200';
                $this->_response['data'] = $response->getCustomerProfileId();
            } else {
                $errorMessages = $response->getMessages()->getMessage();
                $this->_response['error'] = true;
                $this->_response['message'] =  $errorMessages[0]->getText();
                $this->_response['code'] = $errorMessages[0]->getCode();
                $this->_response['data'] = null;
            }
            return true;
        }

        private function _parse_get_response(&$response){
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
            {
                $profileSelected = $response->getProfile();
                $paymentProfilesSelected = $profileSelected->getPaymentProfiles();

                $this->_response['message'] = "Success";
                $this->_response['data'] = ['profile' =>  $profileSelected, 'payment_profiles' => $paymentProfilesSelected];
                $this->_response['error'] = false;
                $this->_response['code'] = '200';
            }
            else
            {
                $errorMessages = $response->getMessages()->getMessage();
                $this->_response['message'] = "Invalid request: ".$errorMessages[0]->getText();
                $this->_response['data'] = null;
                $this->_response['error'] = true;
                $this->_response['code'] = $errorMessages[0]->getCode();
            }
            return true;
        }

        private function _parse_update_customer_profile_response(&$response){
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
            {

                $this->_response['message'] = "Success";
                $this->_response['data'] = ['profile' =>  null, 'payment_profiles' => null];
                $this->_response['error'] = false;
                $this->_response['code'] = '200';
            }
            else
            {
                $errorMessages = $response->getMessages()->getMessage();
                $this->_response['message'] = "Invalid request: ".$errorMessages[0]->getText();
                $this->_response['data'] = null;
                $this->_response['error'] = true;
                $this->_response['code'] = $errorMessages[0]->getCode();
            }
            return true;
        }

        private function _parse_delete_customer_profile_response(&$response){
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
            {

                $this->_response['message'] = "Deleted ";
                $this->_response['data'] = ['profile' =>  null, 'payment_profiles' => null];
                $this->_response['error'] = false;
                $this->_response['code'] = '200';
            }
            else
            {
                $errorMessages = $response->getMessages()->getMessage();
                $this->_response['message'] = "Invalid request: ".$errorMessages[0]->getText();
                $this->_response['data'] = null;
                $this->_response['error'] = true;
                $this->_response['code'] = $errorMessages[0]->getCode();
            }
            return true;
        }
    }
