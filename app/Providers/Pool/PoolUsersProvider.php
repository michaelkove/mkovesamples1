<?php
	/**
	 * Created by PhpStorm.
	 * User: mkova
	 * Date: 10/12/2018
	 * Time: 9:13 AM
	 */

	namespace App\Providers\Pool;
	use App\Repositories\PoolRepository;
	use App\Repositories\EmailInviteRepository;
	use App\Repositories\UserRepository;

	/**
	 * Class PoolUsersProvider
	 * @package App\Providers\Pool
	 */
	class PoolUsersProvider {

		private $_poolRepo;
		private $_inviteRepo;
		private $_userRepo;


		public function __construct(
			PoolRepository $poolRepo,
			EmailInviteRepository $inviteRepo,
			UserRepository $userRepository
		) {
			$this->_poolRepo = $poolRepo;
			$this->_userRepo = $userRepository;
			$this->_inviteRepo = $inviteRepo;
		}

		/**
		 * @param $pool
		 * @param $user
		 * @return mixed
		 */
		public function user_active(&$pool, &$user){
			return $pool->users()->where('users.id', $user->id)->count();
		}

		/**
		 * @param $pool
		 * @param $user
		 * @return mixed
		 */
		public function user_banned(&$pool, &$user){
			return $pool->bad_users()->where('users.id', $user->id)->count();
		}

		/**
		 * @param $pool
		 * @param $user
		 * @return mixed
		 */
		public function user_invited(&$pool, &$user){
			return $this->_inviteRepo->is_invited($user, $pool->id);
		}

		/**
		 * @param $pool
		 * @param $email
		 * @return mixed
		 */
		public function email_invited(&$pool, $email){
			$email = strtolower(trim($email));
			return $this->_inviteRepo->get_by_email_pool_id($email, $pool->id, true);
		}

		/**
		 * @param $pool
		 * @return bool
		 */
		public function user_invite_allowed(&$pool){
			return ($pool->allow_user_invite !== 'disabled');
		}

		/**
		 * @param $pool
		 * @return bool
		 */
		public function auto_approve_users(&$pool){
			return ($pool->allow_user_invite === 'auto');
		}

		/**
		 * @param $pool
		 * @param $user
		 * @return mixed
		 */
		public function user_is_commissioner(&$pool, &$user){
			if(isset($user->id)){
				return $pool->commissioners()->where('users.id', $user->id)->count();
			}
			return $pool->commissioners()->where('users.id', $user)->count();
		}

		/**
		 * @param $pool
		 * @param $user
		 * @return false|mixed
		 */
		public function invited_by_commissioner(&$pool, &$user){
			$invite =  $this->_inviteRepo->get_by_email_pool_id($user->email, $pool->id);
			if($invite){
				$invitingUser = $invite->invited_by_user;
				return $this->user_is_commissioner($pool,$invitingUser);
			}
			return false;
		}

		/**
		 * @param $pool
		 * @param $user
		 * @return bool
		 */
		public function can_user_join(&$pool, &$user){
			if($this->user_banned($pool, $user)){
				return false;
			}
			// Not allowed to invite and not invited by commissioner
			if(!$this->user_invite_allowed($pool) && !$this->invited_by_commissioner($pool, $user)){
				return false;
			}
			return true;
		}

		/**
		 * @param $pool
		 * @param $user
		 * @param string $status
		 * @param false $commissioner
		 * @param null $displayName
		 * @return mixed
		 */
		public function addw_user(&$pool, $user, $status = 'active', $commissioner = false, $displayName = null) {
			$data[$user->id] = [
				'status' => $status,
				'commissioner' => $commissioner,
				'approved' => ($pool->allow_user_invite === 'auto' || $commissioner),
				'display_name' => ($displayName) ?? $user->global_display_name
			];
			$updateUser = $this->update_user($pool, $data);
			$invitedProvider = resolve(\App\Providers\EmailInvite\EmailInvite::class);
			$invitedUser = $invitedProvider->get(['email' => $user->email, 'pool_id' => $pool->id], true)->first();
			if($invitedUser){
				$invitedProvider->update($invitedUser->id, ['accepted' => 'yes', 'pool_users_id' => $user->id]);
			}

			return $updateUser;
		}

		/**
		 * @param $pool
		 * @param $user
		 * @param string $note
		 * @return mixed
		 */
		public function add_user_note(&$pool, &$user, $note = ''){
			$data = [];
			$data[$user->id] = ['user_note' => $note];

			return $pool->all_users()->syncWithoutDetaching($data);
		}

		/**
		 * @param $pool
		 * @param $userId
		 * @param array $note
		 * @return mixed
		 */
		public function add_commissioner_note(&$pool, $userId, $note = []){
			$data = [];
			$data[$userId] = ['commissioner_note' => json_encode($note)];
			return $pool->all_users()->syncWithoutDetaching($data);
		}

		/**
		 * @param $pool
		 * @param $user
		 * @param null $displayName
		 * @param false $commissioner
		 * @param false $forceApprove
		 * @return bool
		 */
		public function add_user(&$pool, &$user, $displayName = null, $commissioner = false, $forceApprove = false){
			if($this->can_user_join($pool, $user)){
				$data[$user->id] = [
					'status' => 'active',
					'commissioner' => $commissioner,
					'display_name' => ($displayName) ?? $user->global_display_name,
				];
				if($this->auto_approve_users($pool) || $forceApprove){

					$data[$user->id]['approved'] = true;
				}
				if($this->user_invited($pool, $user)){

					if($this->invited_by_commissioner($pool, $user)){
						$data[$user->id]['approved'] = true;
					}
					$emailInvite = $this->_inviteRepo->get_by_email_pool_id($user->email, $pool->id);
					if($emailInvite){
						$inviteData = ['accepted' => 'yes', 'pool_users_id' => $user->id];
						$this->_inviteRepo->update($emailInvite->id, $inviteData);
					}
				}
				$pool->all_users()->syncWithoutDetaching($data);

				return true;
			}
			return false;
		}


	}
